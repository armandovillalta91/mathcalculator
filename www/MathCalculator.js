var exec = require('cordova/exec');

exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'MathCalculator', 'coolMethod', [arg0]);
};

exports.testLoop = function (arg0, success, error) {
    exec(success, error, 'MathCalculator', 'testLoop', [arg0]);
};
